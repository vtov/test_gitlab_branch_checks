#!/bin/bash

TOKEN=
GIT_URL=git.example.com
CUR_DATE=$(date '+%Y-%m-%d')
TMP_FILE=tmp_branches.txt
NOTIFIED_NAMES=notified_names.txt
NOTIFIED_DATES=notified_dates.txt

[ -f $TMP_FILE ] && rm $TMP_FILE
[ -f $NOTIFIED_NAMES ] ||  touch $NOTIFIED_NAMES
[ -f $NOTIFIED_DATES ] ||  touch $NOTIFIED_DATES

get_info() {
  GET_PROJECTS="https://$GIT_URL/api/v4/projects"
  PROJECT_LIST=$(curl --silent --header "Authorization: Bearer $TOKEN" $GET_PROJECTS | jq -r '.[].name_with_namespace' | sed "s/ \/ /%2F/g") 

  for PROJECT in $PROJECT_LIST

  do
    GET_BRANCHES="https://$GIT_URL/api/v4/projects/$PROJECT/repository/branches"
      BRANCHES=$(curl --silent --header "Authorization: Bearer $TOKEN" $GET_BRANCHES |jq -r '.[].name' | sed "s/\//%2F/g")
    
    for BRANCH in $BRANCHES  
    do

    BRANCH_INFO_URL="https://$GIT_URL/api/v4/projects/$PROJECT/repository/branches/$BRANCH"
    BRANCH_INFO="$(curl --silent --header "Authorization: Bearer $TOKEN" $BRANCH_INFO_URL )"
    
    BRANCH_NAME=$(echo $BRANCH_INFO |jq -r '.name' )
    BRANCH_LAST_COMMIT=$(echo $BRANCH_INFO | jq -r '.commit.committed_date'| sed 's/T.*//')
    BRANCH_LAST_COMMITTER=$(echo $BRANCH_INFO | jq -r '.commit.committer_email')

    echo $PROJECT  $BRANCH_NAME $BRANCH_LAST_COMMIT $BRANCH_LAST_COMMITTER >> $TMP_FILE

    done
  done
}

check_names() {

cat $TMP_FILE| while read LINE
do
  PR_NAME=$(echo $LINE |awk '{print $1}' |sed "s/%2F/\//g")
  BR_NAME=$(echo $LINE |awk '{print $2}' |sed "s/%2F/\//g")
  BR_LAST_COMMITTER=$(echo $LINE |awk '{print $4}')

  WAS_NOTIFIED=false
  WRONG_BR_NAME=false

  [[ -z "$(fgrep "$LINE" $NOTIFIED_NAMES)"  ]] || WAS_NOTIFIED=true

#  echo $WAS_NOTIFIED

  case $WAS_NOTIFIED in
    true)
      echo "Branch name $BR_NAME in project $PR_NAME has wrong name. Сontributor has already been notified "
    ;;
    
    false)
    [[ $BR_NAME =~ ^(master|develop)$ ]] || [[ $BR_NAME =~ ^(feature|bugfix)\/task-[[:digit:]] ]] || WRONG_BR_NAME=true 
  
    ;;
  esac

  case $WRONG_BR_NAME in
    true)
      echo "You should rename branch $BR_NAME in project $PR_NAME" |mail -s "Rename Branch!"  $BR_LAST_COMMITTER
      echo $LINE >> $NOTIFIED_NAMES
    ;;

    false)
      echo "Branch name $BR_NAME in project $PR_NAME conforms to the rules or its last contributor has already been notified "
    ;;
  esac

done


}


check_date() {

cat $TMP_FILE| while read LINE
do
  PR_NAME=$(echo $LINE |awk '{print $1}' |sed "s/%2F/\//g")
  BR_NAME=$(echo $LINE |awk '{print $2}' |sed "s/%2F/\//g")
  BR_LAST_COMMITTER=$(echo $LINE |awk '{print $4}')
  BR_LAST_COMMIT_DATE=$(echo $LINE |awk '{print $3}')

  WAS_NOTIFIED=false
  LONG_AGO=false

  [[ -z "$(fgrep "$LINE" $NOTIFIED_DATES)"  ]] || WAS_NOTIFIED=true

#  echo $WAS_NOTIFIED

  case $WAS_NOTIFIED in
    true)
      echo "Last commit on $BR_NAME in project $PR_NAME was too long ago . Сontributor has already been notified "
    ;;
    
    false)
    DIFF=$(( ($(date -d $CUR_DATE +%s) - $(date -d $BR_LAST_COMMIT_DATE +%s)) / 86400 ))
    [[ $DIFF -gt 14 ]]  && LONG_AGO=true
  
    ;;
  esac

  case $LONG_AGO in
    true)
      echo "You should upduate or delete branch $BR_NAME in project $PR_NAME" |mail -s "update or delete Branch!"  $BR_LAST_COMMITTER
      echo $LINE >> $NOTIFIED_DATES
    ;;

    false)
      echo "Branch $BR_NAME in project $PR_NAME up to date or its last contributor has already been notified "
    ;;
  esac

done


}


get_info
check_names
check_date















